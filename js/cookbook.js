let cardContainer = document.querySelector('#card-container');
let fullRecipeContainer = document.querySelector('#full-recipe-container');
let searchContainer = document.querySelector('#search-container');
let tagContainer = document.querySelector('#tag-container');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createCard(cardData) {                 //card Data e argument/object od recipeData
  let card = document.createElement('div');
  let cimage = document.createElement('div');
  let title = document.createElement('h5');
  let contents = document.createElement('div');
  let actions = document.createElement('div');

  card.classList.add('card');

  cimage.classList.add('card-image');
  let img = document.createElement('img');
  img.src = './images/' + getRandomInt(1, 29) + '.jpg';
  cimage.appendChild(img);

  title.innerText = cardData.name;

  contents.classList.add('card-content');
  contents.appendChild(title);

 //Exercise 4
  //Here make changes on tags
  cardData.tags.forEach((tag) => {
    let tagLink = document.createElement('a');
    tagLink.innerText = tag
  
    //Filtriranje sprema toa sto sme kliknale
    //na klik ke povikame funkcija
    tagLink.addEventListener('click', function (e) {
      e.preventDefault()
      location.hash = 'tag' + e.target.innerText          //Exercise 6
      let currentTagValue = e.target.innerText                 //variabla za target(kade sme kliknale)
      console.log("Tagot vrz koj kliknavme e", currentTagValue)
      //FILTER funkcija
      let filteredData = recipeData.filter((obj) => {           //filter na objektite od nizata recipeData
                                                                //ke ni vrati filtrirana niza
        return obj.tags.includes(currentTagValue)               // ke ni vrati od objekt kluc "tags" koi go
                                                               //imaat zborot na koj sme kliknale(currentTagValue)
      })
      
      //for za prikazuvanje i kreiranje na filtrirani karticki
      tagContainer.innerHTML = ""   // Za sekoj slucaj pred iteriranjeto na filtered card
                                    //tagContainer go pravime prazen(pred da gi prikaze novite,gi brise starite)
      for (let i = 0; i < filteredData.length; i++){
        let filteredCard = createCard(filteredData[i])
        tagContainer.appendChild(filteredCard)
      }
      console.log("Ova se samo filtriranite tagovi",filteredData )
    })
    contents.appendChild(tagLink);
  });

  actions.classList.add('card-action');
  let link = document.createElement('a');
  link.innerText = "Open Recipe";
  link.classList.add('waves-effect', 'waves-light', 'btn', 'orange');
  link.href = '#' + cardData.id;
  actions.appendChild(link);

  card.appendChild(cimage);
  card.appendChild(contents);
  card.appendChild(actions);

  return card;
};

function renderRecipe() {
  let id = location.hash.replace('#', '');
  let recipe = recipeData.find(r => r.id === id);
  fullRecipeContainer.innerHTML = '';
  
  let wrapper = document.createElement('div');
  let card = document.createElement('div');
  let cimage = document.createElement('div');
  let title = document.createElement('h3');
  let contents = document.createElement('div');

  wrapper.classList.add('full-recipe-wrapper');

  title.innerText = recipe.name;

  cimage.classList.add('card-image');
  let img = document.createElement('img');
  img.src = './images/' + getRandomInt(1, 29) + '.jpg';
  cimage.appendChild(img);
  
  contents.classList.add('card-content');
  let p = document.createElement('p');
  p.innerText = recipe.instructions;
  let h4Instructions = document.createElement('h4');
  h4Instructions.innerText = 'Instructions';

  contents.appendChild(h4Instructions);
  contents.appendChild(p);

  card.classList.add('card');
  card.appendChild(cimage);
  card.appendChild(contents);

  wrapper.appendChild(title);
  wrapper.appendChild(card);

  fullRecipeContainer.appendChild(wrapper);
  
}

//Exercise 6 - return to home page


// Exercise 1 - draw cards on screen using the
// createCard function.

for (let i = 0; i < recipeData.length; i++) {     // For od recept nizata so objecti vo db-recipes.js
  let newCard = createCard(recipeData[i])          //kreiram nova variabla newCard so createCard method na 12 linija
                                                 // ke treba da gi smestime vo HTML card-container
                                                  //na linija 1 go ima kako variabla
  cardContainer.appendChild(newCard)            //gi deodavame,no seuste ne gi prikazuva bidejki vo card-container
                                                 //ima display-none (sokrieni)i ne gi prikazuva
                                                //zatoa ke napravime funkcija handleRoute
 // console.log('Ova e nova karticka', newCard) 
}
//Exercise 3
function handleRoute(e) {
  e.preventDefault()
  let currentHash = location.hash                  //Proverka na momentalniot Hash,
  if (currentHash == "" || currentHash == "#") {   //ako hash e prazen 
    cardContainer.style.display = "flex"          //cardContainer go pravime vidliv
    fullRecipeContainer.style.display = "none"     // a full/site card  se none (sokrieni)
    tagContainer.style.display = "none"           // go krieme
    //exercise 5
  } else if (currentHash.includes('tag')) {    // ako hash sodrzi vo sebe tag(od filtriranjeto)
    cardContainer.style.display = "none"          //da ni gi prekaze samo niv
    fullRecipeContainer.style.display = "none"     //go krieme
    tagContainer.style.display = "flex"
    
  } else {
    cardContainer.style.display = "none"          //go krieme
    fullRecipeContainer.style.display = "flex"     //vo else prikazuvame site recepti
    tagContainer.style.display = "none"           // go krieme
    
    renderRecipe()
  }

}
window.addEventListener('hashchange', handleRoute)   // na clik ja povikuvame handle funkcijata
window.addEventListener('load', handleRoute)  //so ova na reload ke ja povikame funkcijata //handleRoute

//koga ke klikneme na header na vrati prazen hash

document.getElementById('headerElement').addEventListener('click', function (e) {
  e.preventDefault()
  location.hash = "#"
})